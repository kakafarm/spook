;;; SPDX-FileCopyrightText: 2023 Yuval Langer <yuvallangerontheroad@gmail.com>
;;;
;;; SPDX-License-Identifier: AGPL-3.0-or-later

;;; Utility procedures:
(define (alert message)
  (%inline "alert" message))

(define (a) (alert "aaaaa"))

(define (random)
  (%inline "Math.random"))

(define (lerp t start end)
  (+ start
     (* (- end start)
        t)))

(define (random-uniform start end)
  (lerp (random) start end))

(define (randint n)
  "Random integer [0, n)."
  (inexact->exact
   (floor
    (* (random) n))))

(define (filter predicate our-list)
  (cond
   ((null? our-list) '())
   ((predicate (car our-list))
    (cons (car our-list)
          (filter predicate
                  (cdr our-list))))
   (else (filter predicate
                 (cdr our-list)))))

(define (assq-replace alist key value)
  (cons (cons key value)
        (filter (o not
                   (cut eq? key <>)
                   car)
                alist)))

(define (assq-ref alist key)
  (let ((result (assq key alist)))
    (if result
        (cdr result)
        #f))
  #;(alist-ref key alist eq?)
  ;; No alist-ref :(
  ;; https://api.call-cc.org/5/doc/chicken/base/alist-ref
  )

(define (assq-set! alist key value)
  (let ((pair (assq key alist)))
    (set-cdr! pair
              value)))
;;; END Utility procedures.

(define canvas
  (%inline "document.getElementById" "canvas"))

(define ctx
  (%inline ".getContext" canvas "2d"))

;;; Constants:
(define background-fill-style "hsla(0, 0%, 0%, .2)")
(define player-fill-style "rgb(200, 0, 0)")
(define good-brick-fill-style "rgb(50, 100, 0)")
(define bad-brick-fill-style "rgb(200, 0, 0)")
(define cloud-fill-style "#DDE7EE")
(define cloud-fill-style "#bbb")
(define number-of-lanes 3)
(define brick-width (/ (.width canvas) number-of-lanes))
(define brick-half-width (/ brick-width 2))
(define brick-height 40)
(define brick-half-height (/ brick-height 2))
(define player-half-width 20)
(define player-width (* 2 player-half-width))
(define player-half-height 10)
(define player-height (* 2 player-half-height))
(define player-speed-x 0.7)
(define cloud-half-width 75)
(define cloud-width (* 2 cloud-half-width))
(define cloud-half-height 37)
(define cloud-height (* 2 cloud-half-height))
;;; END Constants.

(define (draw-player player)
  (set! (.fillStyle ctx) player-fill-style)
  (draw-rect (- (element-center-x player) player-half-width)
             (- (element-center-y player) player-half-height)
             player-width
             player-height))

(define (draw-rect x y width height)
  (%inline ".fillRect"
           ctx
           x
           y
           width
           height))

(define (clear-rect)
  #;(set! (.fillStyle ctx) background-fill-style)
  (%inline ".clearRect"
           ctx
           0
           0
           (.width canvas)
           (.height canvas)))

(define (cursor-x cursor)
  (car cursor))

;;; Makers:

(define (make-player)
  `((center-x . ,(/ (.width canvas)
                    2))
    (center-y . ,(- (.height canvas)
                    50))
    (half-width . ,player-half-width)
    (half-height . ,player-half-height)
    (velocity-x . 0)
    (velocity-y . 0)))

(define (make-new-cloud)
  (let* ((cloud-y (random-uniform 10
                                  (/ (.height canvas)
                                     5))))
    `((center-x . ,(* -0.999 cloud-width))
      (center-y . ,cloud-y)
      (half-width . ,cloud-half-width)
      (half-height . ,cloud-half-height)
      (velocity-x . 0.05)
      (velocity-y . 0)
      (idspispopd . #f))))

(define (make-new-falling-brick)
  (let* ((zero-one-or-two (randint number-of-lanes)))
    `((center-x . ,(* zero-one-or-two
                      (/ (.width canvas)
                         3)))
      (center-y . ,(/ (.height canvas)
                      10))
      (half-width . ,brick-half-width)
      (half-height . ,brick-half-height)
      (velocity-x . 0)
      (velocity-y . 0.25)
      (idspispopd . #f)
      (is-good . ,(if (= (randint 2) 0)
                      #t
                      #f)))))

;;; END Makers.

;;; Getters:

(define (element-center-x element) (assq-ref element 'center-x))
(define (element-left element)
  (- (element-center-x element)
     (element-half-width element)))
(define (element-right element)
  (+ (element-center-x element)
     (element-half-width element)))
(define (element-center-y element) (assq-ref element 'center-y))
(define (element-top element)
  (- (element-center-y element)
     (element-half-height element)))
(define (element-bottom element)
  (+ (element-center-y element)
     (element-half-height element)))
(define (element-velocity-x element) (assq-ref element 'velocity-x))
(define (element-velocity-y element) (assq-ref element 'velocity-y))
(define (element-width element) (* 2 (element-half-width element)))
(define (element-half-width element) (assq-ref element 'half-width))
(define (element-height element) (* 2 (element-half-height element)))
(define (element-half-height element) (assq-ref element 'half-height))

;;; END Getters.


;;; Setters:

(define (set-element-center-x! element value)
  (assq-set! element 'center-x value))
(define (set-element-center-y! element value)
  (assq-set! element 'center-y value))
(define (set-element-left! element value)
  (assq-set! element
             'center-x
             (+ (element-center-x element)
                (element-half-width element))))
(define (set-element-top! element value)
  (assq-set! element
             'center-y
             (+ (element-center-y element)
                (element-half-height element))))
(define (set-element-velocity-x! element value)
  (assq-set! element 'velocity-x value))
(define (set-element-velocity-y! element value)
  (assq-set! element 'velocity-y value))
(define (set-element-idspispopd! element value)
  (assq-set! element 'idspispopd value))

;;; END Setters.

;;; Predicates:

(define (element-is-good? element) (assq-ref element 'is-good))

(define (element-idspispopd? element) (assq-ref element 'idspispopd))

(define (element-inside-canvas? element)
  (and (<= 0 (element-right element))
       (<= (element-left) (.width canvas))
       (<= 0 (element-bottom element))
       (<= (element-top element) (.height canvas))))

(define (collision? element-a element-b)
  (not (or (element-idspispopd? element-b)
           (< (element-right element-a)
              (element-left element-b))
           (< (element-right element-b)
              (element-left element-a))
           (< (element-bottom element-a)
              (element-top element-b))
           (< (element-bottom element-b)
              (element-top element-a)))))

(define (clip-overshoot target-position old-position new-position)
  (cond
   ((<= old-position target-position new-position)
    target-position)
   ((<= new-position target-position old-position)
    target-position)
   (else
    new-position)))

;;; END Predicates.

;;; Global variables: (~_~)
(define last-loop-time 0)
(define falling-bricks '())
(define next-brickfall-time 0)
(define clouds '())
(define next-cloud-time 0)
(define cursor (cons (/ (.width canvas) 2)
                     (/ (.height canvas) 2)))
(define player (make-player))
(define score 0)
;;; END Global variables.

;;; Drawers:

(define (draw-clouds clouds)
  (set! (.fillStyle ctx) cloud-fill-style)
  (for-each (lambda (cloud)
              (draw-rect (element-center-x cloud)
                         (element-center-y cloud)
                         (element-width cloud)
                         (element-height cloud)))
            clouds))

(define (draw-falling-bricks bricks)
  (for-each (lambda (brick)
              (if (element-is-good? brick)
                  (set! (.fillStyle ctx) good-brick-fill-style)
                  (set! (.fillStyle ctx) bad-brick-fill-style))
              (draw-rect (element-center-x brick)
                         (element-center-y brick)
                         (element-width brick)
                         (element-height brick)))
            bricks))

(define (draw-text)
  (set! (.font ctx) "30px monospace")
  (if (< score 0)
      (set! (.fillStyle ctx) bad-brick-fill-style)
      (set! (.fillStyle ctx) good-brick-fill-style))
  (%inline ".fillText"
           ctx
           score
           10
           50))

(define (draw)
  (clear-rect)
  (draw-clouds clouds)
  (draw-player player)
  (draw-falling-bricks falling-bricks)
  (draw-text))

;;; END Drawers.

(define (clip-low-high value low high)
  ((o (cut min <> high)
      (cut max <> low))
   value))

(define (move-axis dt center velocity)
  (+ center (* velocity dt)))

(define (move-elements! dt elements)
  (for-each (lambda (element)
              (set-element-center-x! element
                                     (move-axis dt
                                                (element-center-x element)
                                                (element-velocity-x element)))
              (set-element-center-y! element
                                     (move-axis dt
                                                (element-center-y element)
                                                (element-velocity-y element))))
            elements))

(define (filter-out-of-canvas-elements elements)
  (filter element-inside-canvas?
          elements))

(define (get-collided-bricks player bricks)
  (filter (cut collision? player <>)
          bricks))

(define (sum our-list)
  (let loop ((our-list our-list)
             (our-sum 0))
    (cond
     ((null? our-list)
      our-sum)
     (else
      (loop (cdr our-list)
            (+ our-sum
               (car our-list)))))))

(define (loop time)
  (let* ((dt (- time last-loop-time)))
    (set! last-loop-time time)

    (move-elements! dt falling-bricks)
    (move-elements! dt clouds)

    ;; Periodically create a new cloud:
    (when (< next-cloud-time
             time)
      (set! clouds (cons (make-new-cloud)
                         clouds))
      (set! next-cloud-time (+ time
                               (random-uniform 5000 8000))))

    ;; Periodically create a new falling brick:
    (when (< next-brickfall-time
             time)
      (set! falling-bricks (cons (make-new-falling-brick)
                                 falling-bricks))
      (set! next-brickfall-time (+ time
                                   (random-uniform 750 1000))))

    ;; Get input:
    (let* ((wanted-player-center-x (cursor-x cursor))
           (previous-player-center-x (element-center-x player))
           (player-direction-x (if (< previous-player-center-x
                                      wanted-player-center-x)
                                   1
                                   -1))
           (new-player-velocity-x (* player-direction-x
                                     player-speed-x)))
      (set-element-velocity-x! player
                               new-player-velocity-x))

    ;; Move player:
    (let* ((old-center-x (element-center-x player))
           (new-center-x (move-axis dt
                                    (element-center-x player)
                                    (element-velocity-x player)))
           (new-center-x (clip-overshoot (cursor-x cursor)
                                         old-center-x
                                         new-center-x))
           (new-center-x (clip-low-high new-center-x
                                        (element-half-width player)
                                        (- (.width canvas)
                                           (element-half-width player)))))
      (set-element-center-x! player
                             new-center-x))

    (set! falling-bricks (filter-out-of-canvas-elements falling-bricks))
    (set! clouds (filter-out-of-canvas-elements clouds))

    (let ((bricks-collided (get-collided-bricks player
                                                falling-bricks)))
      (for-each (lambda (brick)
                  (set-element-idspispopd! brick #t)
                  (if (element-is-good? brick)
                      (begin
                        (set! score (+ score 1)))
                      (begin
                        (set! score (- score 1)))))
                bricks-collided))

    (draw)

    (%inline "window.requestAnimationFrame" (callback loop))))

(%inline "window.requestAnimationFrame" (callback loop))

(for-each (lambda (event-name)
            (%inline ".addEventListener"
                     canvas
                     event-name
                     (callback (lambda (e)
                                 (set! cursor
                                       (cons (.offsetX e)
                                             (.offsetY e)))))))
          '("pointerdown"
            "pointermove"))
