# SPDX-FileCopyrightText: 2023 Yuval Langer <yuvallangerontheroad@gmail.com>
#
# SPDX-License-Identifier: AGPL-3.0-or-later

spock_runtime = pages/spock-runtime-debug-min.js pages/spock-runtime-debug.js \
                pages/spock-runtime-min.js pages/spock-runtime.js

.PHONY: upload all entr clean commit-pages

all: pages/main.js $(spock_runtime)
	chicken-spock main.scm -o pages/main.js

clean:
	rm pages/*js

$(spock_runtime):
	mkdir -p pages && cp `chicken-spock -library-path`/spock-runtime* pages/

pages/main.js: main.scm
	mkdir -p pages && chicken-spock main.scm -o pages/main.js

commit-pages: all
	cp pages/index.html pages/index.html.new
	git checkout pages
	cp pages/*js .
	mv pages/index.html.new index.html
	git add *js *html
	git commit -m f
	git checkout master

upload: commit-pages
	git push pages

entr: all
	ls main.scm | entr -s 'make pages/main.js'
