;;; SPDX-FileCopyrightText: 2023 Yuval Langer <yuvallangerontheroad@gmail.com>
;;;
;;; SPDX-License-Identifier: AGPL-3.0-or-later

;; What follows is a "manifest" equivalent to the command line you gave.
;; You can store it in a file that you may then pass to any 'guix' command
;; that accepts a '--manifest' (or '-m') option.

(specifications->manifest
  (list
   "chicken"
   "coreutils"
   "entr"
   "git"
   "less"
   "make"
   "nss-certs"
   "python"
))
